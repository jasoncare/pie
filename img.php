<?php

// Comment these lines to hide errors
ini_set('display_errors', 0);

$image = $_GET['n'];
$url = 'https://cdn.komikcast.com/wp-content/img/'.$image;
$filename = basename($url);
$fileparts = pathinfo($url);
$name = $fileparts['filename'];
function imagecreatefromfile( $url ) {
    switch ( strtolower( pathinfo( $url, PATHINFO_EXTENSION ))) {
        case 'jpeg':
        case 'jpg':
            return imagejpeg(imagecreatefromjpeg($url));
        break;

        case 'png':
            return imagepng(imagecreatefrompng($url));
        break;

        case 'gif':
            return imagegif(imagecreatefromgif($url));
        break;

        default:
            throw new InvalidArgumentException('File "'.$url.'" is not valid jpg, png or gif image.');
        break;
    }
}
$file_extension = strtolower(substr(strrchr($filename,"."),1));
switch( $file_extension ) {
    case "gif": $ctype="image/gif"; break;
    case "png": $ctype="image/png"; break;
    case "jpeg":
    case "jpg": $ctype="image/jpeg"; break;
    case "svg": $ctype="image/svg+xml"; break;
    default:
}
    header('Content-type: '. $ctype);
    imagecreatefromfile( $url );
init();